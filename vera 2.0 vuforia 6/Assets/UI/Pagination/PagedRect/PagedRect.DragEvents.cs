﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;

namespace UI.Pagination
{
    public partial class PagedRect
    {        
        /// <summary>
        /// Called whenever the user stops dragging
        /// </summary>
        /// <param name="data"></param>
        public void OnEndDrag(PointerEventData data)
        {            
            if (!UsingScrollRect) return;            
            
            if (LoopSeamlessly && !ShowPagePreviews)
            {                    
                // Slightly different logic if we're on the first or last pages
                var pagePosition = GetPagePosition(CurrentPage);
                if (pagePosition == 1 || pagePosition == NumberOfPages)
                {
                    var direction = GetDragDeltaDirection(data);
                    if (direction == DeltaDirection.Next)
                    {                        
                        MoveFirstPageToEnd();                        
                        PreviousPage(); 
                    }
                    else if (direction == DeltaDirection.Previous)
                    {
                        MoveLastPageToStart();                        
                        NextPage();
                    }
                }
                else
                {
                    UpdateSeamlessPagePositions();
                }                
            }
            
            if (HandleDragDelta(data)) return;            

            ScrollToClosestPage();
        }

        protected void ScrollToClosestPage()
        {            
            // If we dragged less than the delta threshold, we may still be between pages - the following code will either return us to our previous page,
            // or take us to the next/previous if they are closer
            var pageDistances = GetPageDistancesFromScrollRectCenter();

            if (pageDistances.Any())
            {
                var closestPage = pageDistances.OrderBy(p => p.Value).First().Key;

                SetCurrentPage(closestPage);
            }
        }

        protected DeltaDirection GetDragDeltaDirection(PointerEventData data)
        {
            bool goToNextPage = false;
            bool goToPreviousPage = false;
            if (ScrollRect.horizontal)
            {
                if (data.delta.x > SwipeDeltaThreshold)
                {
                    goToPreviousPage = true;
                }
                else if (data.delta.x < -SwipeDeltaThreshold)
                {
                    goToNextPage = true;
                }
                else
                {
                    goToPreviousPage = _ScrollRectPosition.x < 0f;
                    goToNextPage = _ScrollRectPosition.x > 1f;
                }                
            }
            else if (ScrollRect.vertical)
            {
                if (data.delta.y > SwipeDeltaThreshold)
                {
                    goToPreviousPage = true;
                }
                else if (data.delta.y < -SwipeDeltaThreshold)
                {
                    goToNextPage = true;
                }
                else
                {
                    goToPreviousPage = _ScrollRectPosition.y < 0f;
                    goToNextPage = _ScrollRectPosition.y > 1f;
                }
            }

            if (goToNextPage) return DeltaDirection.Next;
            if (goToPreviousPage) return DeltaDirection.Previous;

            return DeltaDirection.None;
        }

        protected bool HandleDragDelta(PointerEventData data)
        {            
            var deltaDirection = GetDragDeltaDirection(data);

            return HandleDragDelta(deltaDirection);
        }

        protected bool HandleDragDelta(DeltaDirection deltaDirection)
        {
            if (deltaDirection == DeltaDirection.Next)
            {
                NextPage();
                return true;
            }
            else if (deltaDirection == DeltaDirection.Previous)
            {
                PreviousPage();
                return true;
            }

            return false;
        }
    }
}
