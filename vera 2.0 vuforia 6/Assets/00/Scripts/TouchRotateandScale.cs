﻿using UnityEngine;
using System.Collections;
using System.IO;

public class TouchRotateandScale : MonoBehaviour
{
	private Touch oldTouch1; 
	private Touch oldTouch2;
	private float speed = 0.1f;
	private float oldDis = 0;
	private float newDis = 0;
	private float scaler = 0;

	void Update()
	{
		if (Input.touchCount <= 0) 
		{
			return;
		}
		//Touch Rotate
		/*if (Input.touchCount == 2) 
		{
			Touch touch = Input.GetTouch (1);
			Vector2 deltaPos = touch.deltaPosition;
			transform.Rotate (Vector3.down * deltaPos.x, Space.World);
			//transform.Rotate (Vector3.right * deltaPos.y, Space.World);
		}*/

		//Touch Scale
		Touch newTouch1 = Input.GetTouch(0);
		Touch newTouch2 = Input.GetTouch(1);

		if (newTouch2.phase == TouchPhase.Began)
		{
			oldTouch2 = newTouch2;
			oldTouch1 = newTouch1;
			return;
		}

		float oldDistance = Vector2.Distance(oldTouch1.position, oldTouch2.position);
		float newDistance = Vector2.Distance(newTouch1.position, newTouch2.position);
		oldDis = oldDistance;
		newDis = newDistance;

		float offset = newDistance - oldDistance;

		float scaleFactor = offset / 1f;
		Vector3 localScale = transform.localScale;
		Vector3 scale = new Vector3(localScale.x + scaleFactor,
			localScale.y + scaleFactor,
			localScale.z + scaleFactor);
		scaler = scaleFactor;

		if (scale.x > 10f && scale.y > 10f && scale.z > 10f)
		{
			transform.localScale = Vector3.Lerp(transform.localScale,new Vector3
				(Mathf.Clamp(localScale.x + scaleFactor, 10f, 1000f),
				Mathf.Clamp(localScale.y + scaleFactor, 10f, 1000f),
				Mathf.Clamp(localScale.z + scaleFactor, 10f, 1000f)),10f);
		}
		oldTouch1 = newTouch1;
		oldTouch2 = newTouch2;
	}
}