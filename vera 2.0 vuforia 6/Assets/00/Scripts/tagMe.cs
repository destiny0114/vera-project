﻿using UnityEngine;
using System;
using System.Collections;
using UnityEngine.UI;
using TouchScript.Gestures;

public class tagMe : MonoBehaviour {

	public GameObject fur;
	bool want;

	private void OnEnable()
	{
		GetComponent<TapGesture>().Tapped += tappedHandler;
	}

	private void OnDisable()
	{
		GetComponent<TapGesture>().Tapped -= tappedHandler;
	}

	public void ChangeTag ()
	{
		if(transform.tag =="Untagged")
		{
			transform.tag="furniture";
			want = true;
			GetComponent<Renderer>().material.color= new Color(1f,0.92f,0.015f,1f);
		}

		else if(transform.tag =="furniture")
		{
			transform.tag="Untagged";
			GetComponent<Renderer>().material.color= Color.white;
			want=false;
		}
	}

	private void tappedHandler(object sender, EventArgs eventArgs)
	{
		ChangeTag();
	}
}
