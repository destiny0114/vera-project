﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;

public class characterController : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		transform.Translate (Vector3.left * CrossPlatformInputManager.GetAxis("Horizontal")* 1 * Time.deltaTime);
		transform.Translate (-Vector3.forward * CrossPlatformInputManager.GetAxis("Vertical")* 1 * Time.deltaTime);

	}
	public void scaleMe (float t){
		transform.localScale = new Vector3(t, t, t);
	}
}
