﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.EventSystems;

public class TouchMove : MonoBehaviour
{
	private float speed = 2f;

	public Vector3 startpos;
	Quaternion startrot;

	void Start ()
	{
		//startpos = transform.position;
	}

	void Update ()
	{
		if (Input.touchCount > 0 && Input.GetTouch (0).phase == TouchPhase.Moved) 
		{
			if (!EventSystem.current.IsPointerOverGameObject (Input.GetTouch (0).fingerId)) 
			{
				Vector2 touchDeltaPosition = Input.GetTouch (0).deltaPosition;

				transform.Translate (new Vector3 (touchDeltaPosition.x * speed, 0, touchDeltaPosition.y * speed) * Time.deltaTime, Space.World);
			}
		}
	}
}