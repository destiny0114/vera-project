﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;

public class characterController2 : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		transform.Rotate (Vector3.left * CrossPlatformInputManager.GetAxis("RotationL")* 30 * Time.deltaTime);
	}

	public void scaleMe (float t){
		transform.localScale = new Vector3(t, t, t);
	}
}
