﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIManager : MonoBehaviour 
{

	public GameObject[] num;
	public GameObject[] menu;

	public virtual void DisableNum()
	{
		foreach (GameObject nums in num) 
		{
			nums.gameObject.SetActive (false);
		}
	}

	public virtual void EnableNum()
	{
		foreach (GameObject nums in num) 
		{
			nums.gameObject.SetActive (true);
		}
	}

	public virtual void DisableMenu()
	{
		foreach (GameObject menus in menu) 
		{
			menus.gameObject.GetComponent<Button>().interactable = false;
		}
	}

	public virtual void EnableMenu()
	{
		foreach (GameObject menus in menu) 
		{
			menus.gameObject.GetComponent<Button>().interactable = true;
		}
	}

	public virtual void Fb()
	{
		Application.OpenURL ("fb://page/284798185027489");
	}

}
