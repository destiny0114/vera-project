﻿using UnityEngine;
using System.Collections;

public class FurnituresSelection : MonoBehaviour 
{
	
	public GameObject[] furnitures;
	public Transform point;
	private int index;
	private GameObject selectedfurnitures;

	void Start ()
	{
		selectedfurnitures = Instantiate (furnitures [0], transform.position, transform.rotation) as GameObject;
		selectedfurnitures.transform.SetParent (point, true);
	}


	public virtual void Select (int selectionindex)
	{
		if (selectedfurnitures != null)
			Destroy (selectedfurnitures);

		selectedfurnitures = Instantiate (furnitures [selectionindex], transform.position, transform.rotation) as GameObject;

		selectedfurnitures.transform.SetParent (point, true);

		if (index == furnitures.Length)
			index = 0;
		index++;
	}
}
